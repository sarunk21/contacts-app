import { useState } from 'react';
import Contacts from '../components/Contacts/Contacts';
import Favourite from '../components/Favourite/Favourite';
import styles from './Index.module.scss';

export default function Home() {
  const [searchClick, setsearchClick] = useState(false);

  return (
    <>
      <section className={styles.header} id="header">
        <h1>Contancts</h1>
        <div className={styles.search}>
          <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" className={searchClick ? 'hidden' : 'inline-flex'}><path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z" /></svg>
          <input type="text" className={`w-full ${searchClick ? 'px-2' : 'px-4'}`} onFocus={() => setsearchClick(true)} onBlur={() => setsearchClick(false)} placeholder="Search name ..." />
        </div>
      </section>
      <section className={styles.favourite} id="favorite">
        <h1>Favourite</h1>
        <Favourite />
      </section>
      <section className={styles.contacts} id="contacts">
        <h1>My Contacts (10)</h1>
        <Contacts />
        <div className={styles.add}>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="36" height="36"><path d="M19 8h-2v3h-3v2h3v3h2v-3h3v-2h-3zM4 8a3.91 3.91 0 0 0 4 4 3.91 3.91 0 0 0 4-4 3.91 3.91 0 0 0-4-4 3.91 3.91 0 0 0-4 4zm6 0a1.91 1.91 0 0 1-2 2 1.91 1.91 0 0 1-2-2 1.91 1.91 0 0 1 2-2 1.91 1.91 0 0 1 2 2zM4 18a3 3 0 0 1 3-3h2a3 3 0 0 1 3 3v1h2v-1a5 5 0 0 0-5-5H7a5 5 0 0 0-5 5v1h2z" /></svg>
        </div>
      </section>
    </>
  );
}
